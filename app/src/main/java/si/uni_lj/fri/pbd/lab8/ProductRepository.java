package si.uni_lj.fri.pbd.lab8;

import android.app.Application;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Database;

public class ProductRepository {

    private MutableLiveData<List<Product>> searchResults =
            new MutableLiveData<>();
    private LiveData<List<Product>> allProducts;


    private ProductDao productDao;

    public ProductRepository(Application application){
        ProductRoomDatabase db;
        db=ProductRoomDatabase.getDatabase(application);
        productDao=db.productDao();
        allProducts = productDao.getAllProducts();
    }


    public void insertProduct(final Product newproduct) {
        ProductRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                productDao.insertProduct(newproduct);
            }
        });
    }


    public void deleteProduct(final String name) {
        ProductRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                productDao.deleteProduct(name);
            }
        });
    }

    public void findProduct(final String name) {
        ProductRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                searchResults.postValue(productDao.findProduct(name));
            }
        });
    }

    public LiveData<List<Product>> getAllProducts() {
        return allProducts;
    }

    public MutableLiveData<List<Product>> getSearchResults() {
        return searchResults;
    }

}
