package si.uni_lj.fri.pbd.lab8.ui.main;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.lab8.Product;
import si.uni_lj.fri.pbd.lab8.ProductRepository;

public class MainViewModel extends AndroidViewModel {


    LiveData<List<Product>> allProducts;
    MutableLiveData<List<Product>> searchResults;

    private ProductRepository repository;

    public MainViewModel (Application application) {
        super(application);
        repository = new ProductRepository(application);
        allProducts = repository.getAllProducts();
        searchResults = repository.getSearchResults();

    }

    MutableLiveData<List<Product>> getSearchResults(){
        return searchResults;
    }
    LiveData<List<Product>> getAllProducts(){
        return allProducts;
    }

    public void insertProduct(Product product){
        repository.insertProduct(product);
    }
    public void findProduct(String name){
        repository.findProduct(name);
    }
    public void deleteProduct(String name){
        repository.deleteProduct(name);
    }

}
